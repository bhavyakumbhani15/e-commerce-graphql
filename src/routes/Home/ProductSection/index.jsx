import React, { useState } from "react";
import ProductDetails from "../../../components/ProductDetails";
import SingleProduct from "../../../components/SingleProduct";
import "./ProductSection.scss";
export default function ProductSection(props) {
  const [ toogle , setToogle ] = useState(false);

  return (
    <div>
      <div className="product-section-all-contnet-alignment">
        <div className="container">
          <div className="title">
            <h1>Best Selling Products</h1>
          </div>
          <div className="grid">
            {
              [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17].map(()=> {
                return(
                  <div className="grid-items">
                    <SingleProduct setToogle={setToogle} toogle={toogle}/>
                  </div>
                )
              })
            }
          </div>
        </div>
      </div>
      <ProductDetails setToogle={setToogle} toogle={toogle}/>
    </div>
  );
}
