import React from 'react'
import './CartItems.scss';
import ProductCounter from '../../../components/ProductCounter/index';
export default function CartItems() {
  return (
    <div>
        <div className='cart-items-design'>
            <div className='sub-grid'>
                <div className='img'>
                  <img src="https://ideen-images.s3-ap-south-1.amazonaws.com/1654781391491.jpeg"/>
                </div>
                <div>
                    <div className='two-text'>
                        <p>PINK HOT FASHION BOTTLE TY-5237</p>
                        <p>$11.50</p>
                    </div>
                    <ProductCounter/>
                    <div className='remove-icon'>
                        <i className='fa-solid fa-trash-can'></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}
