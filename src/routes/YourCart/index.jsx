import React from 'react'
import CartItems from './CartItems';
import './YourCart.scss';
export default function YourCart() {
  return (
    <div>
        <div className='your-cart-all-details-alignment'>
            <div className='container'>
                <div className='title'>
                    <h1>Your Cart</h1>
                </div>
                <div className='sub-text'>
                    <span>Cart Items</span>
                </div>
                <div className='grid'>
                    <div className='grid-items'>
                        {
                            [0,1,2,3,4,5,6].map(()=> {
                                return(
                                    <CartItems/>
                                )
                            })
                        }

                        <div className='total-amount'>
                            <p>Total Amount (-10%)</p>
                            <p><del>$35.98</del></p>
                        </div>
                        <div className='total-amount'>
                            <p>Discounted Amount</p>
                            <p className='green'>$32.38</p>
                        </div>
                    </div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
  )
}
