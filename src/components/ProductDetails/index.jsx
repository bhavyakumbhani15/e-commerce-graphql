import React from 'react'
import ProductCounter from '../ProductCounter';
import './ProductDetails.scss';
export default function ProductDetails(props) {

  const { toogle , setToogle } = props 

  return (
    <div>
      {
        toogle && (
          <div className='modal-blur'></div>
        )
      }
      <div className={toogle ? 'moda-md show' : 'moda-md hide' }>
        <div className='modal-header'>
          <span>Product Details</span>
          <i onClick={()=> setToogle(false)} class="fa-solid fa-xmark"></i>
        </div>
        <div className='modal-body'>
          <div className='grid'>
            <div className='grid-items'>
              <div className='lg-image'>
                <img src="https://ideen-images.s3-ap-south-1.amazonaws.com/1654781391491.jpeg"/>
              </div>
              <div className='image-grid'>
               {
                [0,1,2,3].map(()=> {
                  return( <div className='image-grid-items'>
                  <img src="https://ideen-images.s3-ap-south-1.amazonaws.com/1654781391491.jpeg"/>
                </div>)
                })
               }
              </div>
            </div>
            <div className='grid-items'>
              <div className='text-style'>
                <h2>PINK HOT FASHION BOTTLE TY-5237</h2>
                <p>DROP PROOF, SPILL SAFE. BPA FREE</p>
                <h4>$ 11.50</h4>
              </div>
              <div className='counter-details'>
                <ProductCounter/>
              </div>
              <div className='two-text'>  
                <p>Category: <span>Sports</span></p>
                <p>UPC: <span>ACBOT00006</span></p>
              </div>
              <div className='add-to-cart'>
                <button>Add to Cart</button>
              </div>
              <div className='two-text'>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
