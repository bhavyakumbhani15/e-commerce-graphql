import React from 'react'
import './ProductCounter.scss';
export default function ProductCounter() {
  return (
    <div>
        <div className='product-counter-design'>
            <div className='action-button'>
                <i className='fa-solid fa-minus'></i>
            </div>
            <div className='counter'>1</div>
            <div className='action-button'>
                <i className='fa-solid fa-plus'></i>
            </div>
        </div>
    </div>
  )
}
