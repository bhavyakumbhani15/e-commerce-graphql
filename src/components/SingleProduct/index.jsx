import { useState } from 'react';
import ProductCounter from '../ProductCounter';
import ProductDetails from '../ProductDetails';
import './SingleProduct.scss';
export default function SingleProduct(props) {

    const { toogle , setToogle } = props 

  return (
    <>
        <div className='single-product'>
            <div className='card-image'>
                <img src="https://ideen-images.s3-ap-south-1.amazonaws.com/1654781391491.jpeg"/>
            </div>
            <div className='card-details'>
                <h6>PINK HOT FASHION BOTTLE TY-5237</h6>
                <p>DROP PROOF, SPILL SAFE. BPA FREE</p>
                <h5>Price : <span>$ 11.50</span></h5>
                <div className='button-group-alignment'>
                    <ProductCounter/>
                    <button onClick={()=> setToogle(!toogle)}>Add</button>
                </div>
            </div>
        </div>
        
    </>
  )
}
