import React from 'react'
import Topbar from '../Topbar';
import './header.scss';
import CartIcon from '../../../assets/icons/cart.png';
import SearchIcon from '../../../assets/icons/search.svg';
export default function Header() {
  return (
    <>
      <Topbar/>
      <header>
        <div className='container'>
          <div className='header-alignment'>
            <div className='logo'>
              <span>Logo.</span>
            </div>
            <div className='search'>
              <input type="text" placeholder='Search products by ' />
              <div className='search-icon'>
                <img src={SearchIcon} alt="SearchIcon"/>
              </div>
            </div>
            <div className='other-content'>
              <div className='cart-icon-text'>
                <div className='relative-div'>
                  <img src={CartIcon} alt="CartIcon"/>
                  <div className='edge'>1</div>
                </div>
                <p>Shopping Cart</p>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  )
}
